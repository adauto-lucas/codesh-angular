import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductApiService } from '../../api/product-api.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public file: File;
  public selecionarFiles: FileList;
  public products: any = [];
  public formData = new FormData();

  public formUpload : FormGroup;

  constructor(private productApiService: ProductApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.showFileName();
    this.listarProdutos();
   
  }

  public showFileName() {
    $(document).on('change', 'input[type="file"]', function (event) {
      var filename = $(this).val();
      if (filename == undefined || filename == "") {
        $(this).next('.label-upload').html('No file chosen');
      }
      else { $(this).next('.label-upload').html(event.target.files[0].name); }
    });
  }

  public listarProdutos() {
    this.productApiService.getList().subscribe((product) => {
      this.products = product;
    });
  }

  public removerProduto(id: number) {
    if (window.confirm('Tem certeza que deseja deletar este arquivo?')) {
      this.productApiService.deleteProduct(id).subscribe((data) => {
        this.listarProdutos();
      });
    }
  }


  public uploadFileProduto(files: FileList) {
    this.formData.append('products', files[0]);
  }


  onSubmit() {
    this.productApiService.sendFileProduct(this.formData).subscribe(res => {
      alert('Upload efetuado com Sucesso!')
    },
      (error) => {
        alert('Falha ao efetuar o upload!')
      });
  }
}




