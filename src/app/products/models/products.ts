export class Product{
    id: number;
    title: string;
    type: string;
    description: string;
    price: number;
    rating: number;
    createdAt: string;

}  